---
title: N05系(8-10次車)
categories:
  - 東西線
---

所謂"N05系"、前面のデザインが変更になっています

また一部編成では、（おそらく）C修繕工事の一環で

+ 車番の移設
+ 行先表示器のフルカラー化
+ 前照灯のLED化

等が行われていますが、編成によって3色LEDを維持していたりと、見た目に細かな差異が見られます

## 05-125F (25F)
![車番移設前 2015.07.03](25F/20150703_114744.jpg)
![新ROM 2018.12.28](25F/20181228_132253.jpg)
![新ROM+前照灯LED 2022.01.09](25F/20220109_164257.jpg)

1枚目では車番の移設のみ行われ、後日撮影した2枚目では前照灯のLED化も行われています
## 05-126F (26F)
![2016.05.24](26F/20160524_180000.jpg)
![2016.05.18](26F/20160518_163205.jpg)

![車番移設 2018.01.23](26F/20180123_133502.jpg)
![車番移設+前照灯LED 2022.03.05](26F/20220305_151228.jpg)

## 05-127F (27F)
![2017.04.13](27F/20170413_131143.jpg)
![前照灯LED+新ROM 2024.05.20](27F/20240520_175024.jpg)

## 05-128F (28F)
![2016.07.06](28F/20160706_182259.jpg)
![前照灯LED化+車番移設+新ROM 2024.06.03](28F/20240603_115843.jpg)
## 05-129F (29F)
![2016.05.18](29F/20160518_181946.jpg)
![前照灯LED+新ROM 2022.01.09](29F/20220109_171359.jpg)

![前照灯LED+新ROM 2024.05.20](29F/20240520_175306.jpg)

## 05-130F (30F)

![車番移設+新ROM 2018.12.28](30F/20181228_133757.jpg)
![車番移設+新ROM+前照灯LED 2022.09.18](30F/20220918_165944.jpg)

25Fに同じパターン

## 05-131F (31F)
![2016.05.24](31F/20160524_161627.jpg)
![車番移設+前照灯LED+行先表示フルカラー化 2022.01.09](31F/20220109_162435.jpg)

## 05-132F (32F)
![車番移設前+行先表示3色 2017.04.20](32F/20170420_134704.jpg)
![車番移設+行先表示フルカラー化 2019.03.12](32F/20190312_174514.jpg)

行先表示器がフルカラー化で前照灯がLED化されていないのは、あまり見ないパターンですね

## 05-133F (33F)
![2016.07.11](33F/20160711_155559.jpg)
![平日朝夕の総武線直通運用 2016.05.18](33F/20160518_180608.jpg)

![車番移設+前照灯LED+行先表示フルカラー化 2024.05.12](33F/20240512_120140.jpg)
![南砂町駅の工事時には葛西行きが運行された 2024.05.11](33F/20240511_090002.jpg)