---
title: 209系2000番台/2100番台行先表示器
categories:
  - 行先表示
draft: false
date: 2024-08-04
---


| 画像                               | 文字列           | 備考(運行本数は2024年時点)           |
| ---------------------------------- | --------------- | ---------------------------------- |
| ![](uchibo-line.webp)               | 内房線           |                                    |
| ![](sotobo-line.webp)               | 外房線           |                                    |
| ![](sotobo-togane-line.webp)        | 外房・東金線     |                                    |
| ![](sobu-mainline.webp)             | 総武本線         |                                    |
| ![](narita-line.webp)               | 成田線           |                                    |
| ![](uchibo-line_chiba.webp)         | 内房線 千葉      |                                    |
| ![](uchibo-line_kimitsu.webp)       | 内房線 君津      |                                    |
| ![](sotobo-line_mobara.webp)        | 外房線 茂原    |                                      |
| ![](sotobo-line_kazusa-ichinomiya.webp) | 外房線 上総一ノ宮 |                                |
| ![](sotobo-line_ohara.webp)         | 外房線 大原      |                                    |
| ![](sotobo-line_katsuura.webp)      | 外房線 勝浦      | 夜間帯を中心に運行                   |
| ![](sotobo-togane-line_togane.webp) | 外房・東金線 東金 | 209系による運用は1日1本             |
| ![](narita-line_sawara.webp)        | 成田線 佐原      | 209系による運用は1日2本              |
| ![](narita-line_choshi.webp)        | 成田線 銚子      | 早朝・夜を中心に運行                 |
| ![](sobu-mainline_chiba.webp)       | 総武本線 千葉    |                                    |
| ![](sobu-mainline_naruto.webp)      | 総武本線 成東    |                                    |
| ![](sobu-mainline_yokoshiba.webp)   | 総武本線 横芝    | 1日1本運行                          |
| ![](sobu-mainline_choshi.webp)      | 総武本線 銚子   |                                     |
| ![](out-of-service.webp)            | 回送            |                                     |
<!--撮りたいもの

### 内房線
+ 内房線 姉ヶ崎（6・18時台に1本ずつ）
+ 内房線 木更津（8・10・12・13・14・18時台）
+ 内房線 上総湊（朝3本、14~18時台に1本ずつ）
+ 内房線 館山（19・20・22時台）
+ 内房線 千倉（木更津始発が21時台に1本）
+ 内房線 安房鴨川（定期では消滅？）
+ 内房線 大貫（表示自体はあるらしい）

### 外房線
+ 外房線 千葉
+ 外房線 誉田（6・17時台に1本ずつ）
+ 外房線 安房鴨川（17・19・20・22時台に1本ずつ）
+ 外房・東金線 成東（午前は概ね1時間に1本、午後は12・13・15・18時台）

### 東金線
+ 東金・外房線 千葉
+ 東金線 大網
+ 東金線 成東

### 総武本線
+ 総武本線 四街道（17・18・19時台に1本）
+ 総武本線 佐倉（9時台に1本）
+ 総武本線 八街（23時台に1本）
+ 総武本線 旭（19時台に銚子発の1本のみ）

### 成田線
+ 成田線 千葉
+ 成田線 成田（千葉発は7~10・12・14・19・23・0時台）
+ 成田線 成田空港（7・11・13・15・16・20時台に1本ずつ）

### 鹿島線（定期運用消滅）
+ 成田・鹿島線 鹿島神宮
+ 成田・鹿島線 鹿島サッカースタジアム
+ 鹿島線 佐原
+ 鹿島線 鹿島神宮
+ 鹿島線 鹿島サッカースタジアム
+ 鹿島・成田線 千葉
+ 鹿島・成田線 佐倉
+ 鹿島・成田線 成田

### 種別のみ
+ 内房線 各駅停車
+ 外房線 各駅停車
+ 総武本線 各駅停車
+ 成田線 各駅停車
+ 鹿島線 各駅停車

-->