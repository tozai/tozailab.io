---
title: E233系5000番台(京葉線)行先表示器
categories:
  - 行先表示
draft: true
---
## 路線名あり

| 画像                                    | 文字列           | 備考                       |
| ------------------------------------- | ------------- | ------------------------ |
| ![](keiyo-local_tokyo.webp)           | 京葉線各駅停車 東京    | 折返し駅到着直後や発車直後は、次駅案内を省略する |
| ![](keiyo-local_for-tokyo.webp)       | 京葉線各駅停車 東京行   | 上記を除いて原則、次駅案内を併記する       |
| ![](keiyo-local_for-soga.webp)        | 京葉線各駅停車 蘇我行   |                          |
| ![](keiyo-local_for-kimitsu.webp)     | 京葉線各駅停車 君津行   |                          |
| ![](keiyo-local_via-uchibo-line.webp) | 京葉線各駅停車 内房線直通 |                          |
| ![](keiyo-rapid_tokyo.webp)           | 京葉線快速 東京      |                          |
| ![](keiyo-rapid_for-katsuura.webp)    | 京葉線快速 勝浦行     | 2024年3月改正で廃止             |
| ![](keiyo-rapid_via-togane-line.webp) | 京葉線快速 東金線直通   | 2024年3月改正で廃止             |

## 路線名無し

| 画像                                    | 文字列         | 備考                             |
| ------------------------------------- | ----------- | ------------------------------ |
| ![](local_for-kazusa-ichinomiya.webp) | 各駅停車 上総一ノ宮行 | 直通先の外房線など京葉線外では、路線名が非表示となる     |
| ![](local_for-ooami.webp)             | 各駅停車 大網行    |                                |
| ![](futsu_nishifunabashi.webp)        | 普通 西船橋      | 朝夜中心に運行される西船橋行きは、例外的に「普通」表示となる |
