---
title: 京急(幕車) 行先表示器
categories:
  - 行先表示
draft: true
---
| 画像                                                         | 文字列(太字は定期運用無し)        | 備考                        |
| ---------------------------------------------------------- | --------------------- | ------------------------- |
| ![](limited-exp-kaitoku_keikyu-kawasaki.webp)              | **快特 京急川崎**           | ダイヤ乱れ時に発生                 |
| ![](limited-exp-kaitoku_kanazawa-bunko.webp)               | 快特 金沢文庫               |                           |
| ![](limited-exp-kaitoku_keikyu-kurihama.webp)              | 快特 京急久里浜              |                           |
| ![](limited-exp_aoto.webp)                                 | 特急 青砥                 |                           |
| ![](limited-exp_miura-kaigan.webp)                         | 特急 三浦海岸               | 深夜に1本のみ                   |
| ![](airport-express_shinagawa.webp)                        | **エアポート急行 品川**        | エアポート急行は廃止となりました          |
| **![](airport-express_keikyu-kamata_haneda-airport.webp)** | **エアポート急行 京急蒲田↔羽田空港** | 実際には存在し得ないがイベント等で見るチャンス有り |
| ![](local_aoto.webp)                                       | 普通 青砥                 |                           |
| ![](local_takasago.webp)                                   | 普通 高砂                 |                           |
| ![](not-in-service.webp)                                   | 回送                    |                           |
| ![](charteded.webp)                                        | **貸切**                | 久里浜イベント時にシャトル便として運行       |
|                                                            |                       |                           |
