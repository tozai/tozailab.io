---
title: 東西線05系・07系・東葉2000系行先表示器(フルカラーLED)
categories:
  - 行先表示
  - 東西線
---

仕様が各車共通と思われるため1つのページに纏めています

| 画像                                               | 文字列(太字は定期運用無し) | 備考                                                                                 |
| ------------------------------------------------ | -------------- | ---------------------------------------------------------------------------------- |
| ![](local_mitaka.webp)               | 各駅停車・三鷹        |                                                                                    |
| ![](local_nakano.webp)               | 各駅停車・中野        |                                                                                    |
| ![](local_kudanshita.webp)           | 各駅停車・九段下       | 平日朝に1本のみ                                                                           |
| ![](local_toyocho.webp)              | 各駅停車・東陽町       | 平日朝ラッシュ後に多く見られるが、深夜帯にも少数存在                                                         |
| ![](local_myoden.webp)               | 各駅停車・妙典        | 平日朝ラッシュ後に多く見られるが、深夜帯にも少数存在                                                         |
| ![](local_nishi-funabashi.webp)      | 各駅停車・西船橋       |                                                                                    |
| ![](local_yachiyo-midorigaoka.webp)  | 各駅停車・八千代緑が丘    | 平日朝に2本、土休日深夜に東葉勝田台発の列車が1本存在                                                        |
| ![](local_toyo-katsutadai.webp)      | 各駅停車・東葉勝田台     |                                                                                    |
| ![](local_tsudanuma.webp)            | 各駅停車・津田沼       | 平日朝に数本、夕方に1本存在                                                                     |
| ![](local_takadanobaba.webp)         | **各駅停車・高田馬場**  | 2022年の線路冠水時に発生                                                                     |
| ![](local_nihombashi.webp)           | **各駅停車・日本橋**   | 2022年の線路冠水時に発生                                                                     |
| ![](local_kayabacho.webp)            | **各駅停車・茅場町**   | 南砂町駅の線路切替工事で発生                                                                     |
| ![](local_kita-narashino.webp)       | **各駅停車・北習志野** | 年に1日、東葉勝田台～北習志野間で運行（花火大会に伴う増発）                                    |
| ![](local_kasai.webp)                | **各駅停車・葛西**    | 南砂町駅の線路切替工事で発生                                                                     |
| ![](rapid_mitaka.webp)               | 快速・三鷹          | 平日・土休日ともに朝に1本、平日夕～夜に数本存在                                                           |
| ![](rapid_toyo-katsutadai.webp)      | 快速・東葉勝田台       |                                                                                    |
| ![](rapid_tsudanuma.webp)            | 快速・津田沼         | 平日夕方に数本存在（朝の1本はJR車）                                                                |
| ![](rapid_kudanshita.webp)           | **快速・九段下**     | 時差BIZトレインとして臨時で運行された実績がある                                                          |
| ![](rapid_nishi-funabashi.webp)      | 快速・西船橋         | メトロ車による定期運用は存在しないが、ダイヤ乱れによる直通運転中止時に突発的に発生する                                        |
| ![](rapid_yachiyo-midorigaoka.webp)  | **快速・八千代緑が丘**  | [ダイヤ乱れ時に入庫列車の調整により発生した事例がある模様・・・](https://youtu.be/wY3t1YoSgDE) 筆者は車両基地内でのテスト表示を目撃 |
| ![](com-rapid_mitaka.webp)           | 通勤快速・三鷹        | 平日朝のみ運行                                                                            |
| ![](com-rapid_nakano.webp)           | 通勤快速・中野        | 平日朝のみ運行                                                                            |
| ![](toyo-rapid_toyo-katsutadai_macron.webp) | **東葉快速・東葉勝田台** | 東葉快速は、2014年のダイヤ改正をもって廃止されました                                                       |
| ![](toyo-rapid_toyo-katsutadai.webp) | **東葉快速・東葉勝田台** | 現行ではマクロン（長音）無しの純粋なローマ字表記となっている                                                       |
| ![](toyo-rapid_tsudanuma.webp)       | 東葉快速・津田沼       | 東葉快速が復活しても運用上存在し得ないが、種別と行先のデータが独立して存在するため表示自体は可能                                   |
| ![](via-tozai-line.webp)             | 東西線直通          | JR線・東葉高速線内や、中野駅の始発列車で行先と交互に表示する                                                    |
| ![](via-jr-chuo-line.webp)           | JR中央線直通        | 三鷹行きの列車は行先と交互に表示する                                                                 |
| ![](via-jr-sobu-line.webp)           | JR総武線直通        | 津田沼行きの列車は行先と交互に表示する                                                                |
| ![](not-in-service.webp)             | 回送             |                                                                                    |
| ![](test-run.webp)                   | **試運転**        |                                                                                    |
| ![](notype_mitaka.webp)              | 三鷹             | 現在は「各駅停車」の種別表示を行うようになった為、今は見ることが出来ない                                               |
| ![](notype_nakano.webp)              | 中野             | 上記と同様です、以下しっかり撮っておけば良かったと後悔するシリーズが続きます・・・                                          |
| ![](notype_toyocho.webp)             | 東陽町            |                                                                                    |
| ![](notype_myoden.webp)              | 妙典             |                                                                                    |
| ![](notype_nishi-funabashi.webp)     | 西船橋            |                                                                                    |
| ![](notype_yachiyo-midorigaoka.webp) | 八千代緑が丘         | 旧ROMシリーズでは比較的撮影難易度が高め (当時はメトロ車運用が平日朝1本のみ)                                          |
| ![](notype_toyo-katsutadai.webp)     | 東葉勝田台          |                                                                                    |
| ![](notype_tsudanuma.webp)           | 津田沼            |                                                                                    |
| ![](via-jr-line.webp)                | JR線直通          | 新ROMでは「JR中央線直通」「JR総武線直通」と路線名を冠するよう修正された                                            |

