---
title: 引退後の05系初期車
date: 2011-11-16
categories: [東西線, 行徳分室]
tags: [東京メトロ東西線]
---

![05-106F 05K 原木中山](20111116_1.jpg)
![中間車の妻面が拝める状態で留置](20111116_2.jpg)

![05-113F 行徳](20111116_3.jpg)

15000系の投入により、05系初期車は東西線から引退しましたが、暫くの間は行徳分室で留置されていました

編成は分割された状態で留置されていました

行先表示がそれぞれ原木中山、行徳となっていましたが、現在どちらの駅にも渡り線は存在しません

行徳行きの列車は、妙典駅が開業する前に入庫運用として存在していました

また、原木中山駅にはかつて渡り線が存在していました

理由としては、国鉄でストライキが発生した際に、西船橋駅が使用できなくなる可能性がある為その備えとして・・・と言われていますが、文献はちょっと確認が出来ていません

留置されていたこの06Fと13Fは、ともに千代田線北綾瀬支線用へ転用改造されています
