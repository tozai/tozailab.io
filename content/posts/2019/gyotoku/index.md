---
title: 東葉車と並ぶ03系
date: 2019-03-12
categories:
  - 行徳分室
  - 東西線
tags:
  - 東京メトロ東西線
  - 東京メトロ日比谷線
  - 東葉高速鉄道
---

![](20190312_150312.jpg)
![](20190312_151628.jpg)

![](20190312_151719.jpg)

今回行徳分室へやってきた目的は疎開中の03系についてです

現地へ赴くと、03系03-132F・03-133Fの先頭車がそれぞれ留置されていました

東葉2000系と並ぶ光景は、この疎開によるものがおそらく史上初でしょう・・・

05系・E231系800番台との並びも見られましたが、構図上どうしても障害物が入り込んでしまいました

当時はまだ妙典橋が開通しておりませんでしたから、見えている車両の更に後ろへ留置されている車両の編成番号は確認出来ませんでした

それにしても、2線を専有して疎開とは中々に贅沢ですね・・・

15000系が増発目的で増備されるなど、ここ最近の東西線車両の編成数は純増傾向にありますが、それを考慮してもキャパに余裕があるという事でしょうか？

更に5000系置き換え時まで遡ると、N05系4編成の増備が中止された際に代替として07系が東西線へ編入しましたが、こちらは6編成の編入となっており全体では2編成増加しました

東西線の全通当初は人口希薄な地帯でしたから、恐らく土地には余裕があったのでしょう（？）

この後付近に咲く河津桜と絡めた撮影も行いましたが、そちらはあまりいい写真が残っておらずボツとしました
