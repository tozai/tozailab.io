---
title: TX総合基地脱線事故による廃車
date: 2019-02-28
tags:
  - つくばエクスプレス
---

![](20150614_154057.jpg)

2019年2月28日に、つくばエクスプレス総合基地内にてテスト走行中のTX-2000系第71編成が車止めへ衝突し、脱線しました

当該編成の第71編成は運用復帰すること無く、2171・2272・2371号車は廃車、解体となりました

残る3両については、廃車扱いか否かについては不明なものの解体を免れ、車両基地内で留置されています

[Googleマップ](https://maps.app.goo.gl/CUMoTf7mUTZM8rPPA)で車両基地を参照すると、解体を免れた3両が独立した線路上に留置されている姿を確認することが出来ます

<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d946.7563665361724!2d139.99548115997527!3d35.96407295771565!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sja!2sjp!4v1714807496527!5m2!1sja!2sjp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
