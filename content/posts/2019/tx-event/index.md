---
title: つくばエクスプレスまつり2019
date: 2019-11-03
tags:
  - つくばエクスプレス
---

2019年11月3日に、つくばエクスプレスまつりが開催されました

![](20191103-1.jpg)
![](20191103-2.jpg)

イベントでは、製造されたばかりのTX-3000系が初めて公開されました

~~TX-1000系たちが斬新なデザインだった分、良くも悪くもありがちなデザインに感じでしまう~~

![](20191103-3.jpg)
![](20191103-4.jpg)

つくばエクスプレスの行先表示は全駅分用意されているらしいので、折り返し運転が物理的に不可能な駅についても、しっかりと表示が用意されています

![](20191103-5.jpg)
![](20191103-6.jpg)

こういう時にしか見れない保線車両たち

![](20191103-7.jpg)
![](20191103-10.jpg)

![](20191103-8.jpg)
![](20191103-9.jpg)

工場の中はかなり広い・・・！
