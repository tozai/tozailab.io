---
title: 代走運用 - 20220109
date: 2022-01-09
categories:
  - 希少運用
  - 東西線
tags:
  - 東京メトロ東西線
  - 東葉高速鉄道
---

![2101F 15S 快速中野](20220109_1.jpg)
![05-131F 64T 快速中野](20220109_2.jpg)

東葉高速線が一時運転見合わせとなった為、いくつか代走運用が見られました

本当は東葉車の快速西船橋表示を狙っていたのですが、私が駅に向かった頃には直通運転が再開されていました
