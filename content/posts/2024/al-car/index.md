---
title: アルミリサイクルカーがB修に入ったらしい
date: 2024-04-06
categories: [東西線]
tags: [東京メトロ東西線]
---

ネットサーフィンしてて今更知ったのですが、どうやら05系24F(05-124F、通称アルミリサイクルカー)が2024年2月に運用離脱したようです

24Fは05系最後の未更新車だったので、とうとう原形顔が消滅してしまいました・・・

![](20240406_1.jpg)

唯一マトモに撮影できた走行写真

なのに東西線外っていう

東葉高速線は高いのであまり足を踏み入れませんでしたが、東西線では見られない構図で撮れる場所が結構あるので、まあ面白いっちゃ面白いんですよね

![](20240406_2.jpg)
![](20240406_3.jpg)

他に一眼レフで撮影する機会があったのは、妙典に寄った時くらいでした

障害物が結構入ってしまっていますが、写っている05系がどれも違う形状をしているのでこれもまた面白い写真になりました

![](20240406_4.jpg)

アルミリサイクルカーとはとことん縁がなく、一眼レフ購入以前の写真も掘り返してみたのですが、掲載できそうな写真はこれ1枚のみでした

辛うじて、メトロマーク貼り付け時代の写真が残っていただけまあいいか、ということで・・・

うーん・・・

幼少期から馴染みのあった車両がいつの間にか消えてしまい、少々寂しさを感じつつも、一方であまり実感が湧いておりません・・・

消えると言っても廃車になる訳ではなく、見た目や内装がリニューアルされた後に帰ってきてくれるので、首を長くして待つことにします

B修繕後もアルミリサイクルカーのステッカーを貼ってくれたら面白いんですけど、こればっかりはどうなるかわかりませんね～

---

![](20151209_24f.jpg)

後から追加で画像を発掘

辛うじて順光の写真がありました

後ろが2~3両入っていないのが減点ポイントですが、無いよりはヨシ！

