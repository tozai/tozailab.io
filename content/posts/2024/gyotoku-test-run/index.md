---
title: 東葉2104F構内試運転
date: 2024-05-24
categories: [東西線, 行徳分室]
tags: [東京メトロ東西線, 東葉高速鉄道]
---

![](20240524_162834.jpg)
![](20240524_163027.jpg)

![](20240524_164953.jpg)
![](20240524_165415.jpg)

またしても行徳分室へ赴きました

夕方の出庫する列車を撮影する為にやってきましたが、東葉2000系が何やら構内で試運転をしておりまして・・・**東葉快速|津田沼という中々にパンチの効いた表示を掲出していました**

東葉快速が仮に復活したとして、津田沼を経由するルートは実現し得ないため絶対に見ることの無い組み合わせです

しかし、昨今のフルカラーLED表示器を搭載した車両では、種別と行先のデータを独立して持っているケースが多い為、このように自由な組合せで掲示することが可能になっています

また、東葉車の行先表示器がフルカラー化されたのは東葉快速の廃止後であるため、オレンジ色の東葉快速表示と東葉車の組合せ自体が、希少な光景ですね

中の人の趣味・もしくは遊び心・はたまたサービス・ただの気まぐれ・どのような意図であるかは神のみぞ知る真実ですが、存分に写真へ収めさせて頂きました・・・

---

![](20240524_170655.jpg)
![](20240524_155848.jpg)

この後に東葉車の出庫運用がある為、てっきりこの2104Fが運用へ就くものだと思っていましたが代わりに15000系が代走で出庫して来ました　これは予想外でした

また1時間ほど前にも、別の出庫する列車を撮影しました

こちらも15000系だったのですが、**なんと右側通行で出庫していきました・・・**

もしかして入出庫線って単線並列なのでしょうか、今までず～～っと普通の複線+αだと思っていましたが此処にて新たな知見を得ました

行徳分室へは何度も足を運んでいますが、新しい発見が尽きません

本当はJR車の出庫も撮影予定だったのですがギリギリ間に合いませんでした、こちらは別の日にまたチャレンジすることとします
