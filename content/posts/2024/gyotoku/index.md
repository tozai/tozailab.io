---
title: 入庫列車撮影&見慣れぬ保線車両たち
date: 2024-05-10
categories: [東西線, 行徳分室]
tags: [東京メトロ東西線, 東葉高速鉄道]
---

## 河川敷から橋梁を観る
![](20240510_112700.jpg)
![](20240510_123444.jpg)

行徳分室へは約2年ぶりに訪れました

まずは河川敷から鉄橋方面にカメラを向けてみます

07-101Fと07-104Fを比較してみると、07-101Fでは窓下・ドア横の2箇所に車番が表記されていますが、07-104Fでは窓下のみとなっています

製造時は窓下のみに表記されていましたがホームドア設置によって隠れてしまう為、後からドア横にも表記が追加されました

このような動きは東西線だけで無く、他の鉄道会社(京急など)でも見られる動きです

勿論15000系等でも同様の処置が行われており、2年半ほど前にドア横表記の追加を確認していますが、07-104Fは何故か処置が行われていないようです・・・

## 入庫列車撮影
![](20240510_115228.jpg)
![](20240510_115457.jpg)

平日午前でしたので、入庫する列車を撮影するチャンスだと思い、跨線橋の上で待機しました

E231系800番台のトップナンバーがやって来ましたが、JR車の妙典行きは確か平日にこの1本だけですから、中々に珍しいです

![](20240510_110607.jpg)
![](20240510_114254.jpg)

土手から望遠でも狙ってみましたが、こちらは高い柵が写り込んでしまうので記録目的としてはイマイチです

入庫後、跨線橋の西側へ回り込むと、05-126Fが同じ列番を表示して止まっていました

~~列番が70S、なんと偶数です・・・メトロ車運用の列番は02Sを除いて基本的に奇数なので、こういう場所でしか見られません~~ 回送ならそんなに珍しくないか・・・

## 見慣れぬ保線車両たち
![](20240510_110657.jpg)
![](20240510_110753.jpg)

南に移動し、門の前まで来ました

柵の網目も入口の門の隙間もあまり大きくないので、レンズ口径の大きな普通のカメラでは撮影し辛いですが、望遠レンズを搭載している携帯電話だといい感じに写すことができます

クレーンを搭載する青い保線車両は結構見かけるタイプですね、今回は撮影しやすい場所に止まっていたこの1台のみ撮影しましたが、奥にも同じ車両が2台ほど留置されていました

さらに少し南側に向かうと・・・なんだこれは・・・

何らかの重機を積載できそうなトレーラーが止まっていました

こちらは初めて見かけましたが、見た目の汚れ具合からしても結構新しめな感じがしますね

![](20240510_121357.jpg)
![](20240510_121753.jpg)

東側へ回るとまた別の保線車両が止まっていました

こちら側の線路は個人的にお役御免の車両(3両化した5000系、引退した05系幕車や疎開の03系)が停車しているイメージが強いので、これまた良い収穫でした

![](20240510_115506.jpg)
![](20240510_114145.jpg)

ここ数年、堤防の嵩上げ工事によって景色が変化しつつあり、このように上から見下ろす形で見ることができようになりました

東葉2000系がアントと繋がって留置されていました

この後、アントが少し動いて2000系を牽引していたのですが、1~2mだけ移動しすぐ終わってしまいました

行徳分室周辺へは計1時間半ほど滞在し、満足したので退散しました
