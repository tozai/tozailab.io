---
title: ケヨ34編成 木更津行き充当
date: 2024-07-21
tags:
  - JR東日本
---
![](20240719_213422.webp)

![](20240719_213106.webp)
![](20240719_213108.webp)

内房線直通列車の間合い運用として、京葉線車両による木更津行きが夜間に存在しますが、7/19にはケヨ34編成が充当されました

京葉車両センター所属の209系のうち、10連はケヨ34編成のみですので貴重です

また、この木更津行き運用は千葉行きの折返しですが、千葉駅入線時点で「木更津」表示に切り替わってしまっていた為、「千葉」表示は記録することが出来ませんでした・・・

こちらは別の日にリベンジしたい・・・

現在、中央線のE233系はトイレ設置工事に伴って予備車が多めに確保されていますが、後に余剰となったE233系が京葉線へ転属してくるという噂もあります

従って、ケヨ34編成の余命はあまり長くないと考えられます

ケヨ34編成が元気なうちに、他の希少運用も抑えておきたい所です
