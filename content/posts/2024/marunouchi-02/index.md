---
tags: [東京メトロ丸ノ内線]
title: "丸ノ内線02系引退" 
date: 2024-04-10
---

ひっそりと引退していましたね

3月31日をもって、最後の1編成として残っていた02-101Fが運用を離脱したとのこと

![](20240410_20240313.jpg)

運用離脱の2週間前くらいに少し東京をうろついていたので、その際に一応写真に収めていました・・・が、流石に機材の限界を感じます

![](20240410_20160611_2.jpg)

これだけだと寂しいので、過去の写真を振り返ります

7年以上前、今使用している一眼レフを買ったばかりの頃に一度遭遇していました

LEDが若干切れていますがバッチリ捕獲済みです

よく見ると、まだ窓の左下に編成番号が貼られていませんね

![](20240410_20191125.jpg)

折角なので他のバリエーションも載せてみる

こちらは前照灯がLEDに交換されています

![](20240410_20160611.jpg)

馴染みのある原形スタイル

サインウェーブも格好いいけどコレもコレで安心感

02系も01系のように地方路線へ飛ばされたら面白いんですけど、第三軌条が厄介だし普通03系にしますよね・・・って

うーん、もう動いてる姿は見られないのかな、01系みたいに保存されたら面白いんですけどね・・・

36年間お疲れ様でした・・・