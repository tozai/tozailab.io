---
title: 第1回 南砂町駅線路切替工事 Part.3
date: 2024-05-12
categories: [東西線, 南砂町駅線路切替工事]
---

## 茅場町行き列車
![](20240511_111348.jpg)
![](20240511_114455.jpg)

![](20240511_124337.jpg)
![](20240511_150654.jpg)

恐らく一番の目玉である、茅場町行き列車です

午前に順光の場所といえばJR線内だよなぁ・・・と思いまして、荻窪の方まで出向いてきました

遅めの時間でしたから、すぐに日が回ってしまい逆光気味となってしまいましたが、到着後すぐに撮影したJR車運用については正面順光で無事頂きました

地下でも一応記録しておこうと、その後は飯田橋駅へ向かいましたが混雑していた為早めに退散しました

## JR車による東陽町行き列車
![](20240511_122913.jpg)

東陽町行きは平日朝などに見られますが、JR車による定期運用については少なくとも今現在のダイヤでは存在しないようです

当日になるまで考慮しておらず、これはこれで良い収穫となりました

## 行先表示

![](20240511_151649.jpg)
![](20240511_125851.jpg)

![](20240511_111410.jpg)
![](20240511_141931.jpg)

普段見られない茅場町行き3種（3色LED、フルカラーLED、JR車）に加え、JR車の東陽町行きを収めることが出来ました

まだ記録したことのない側面表示は、あとは大手町や門前仲町でしょうか・・・こちらは折返し用の渡り線が存在しないため、出現確率が低いです

が、よくよく考えてみると、今回門前仲町行きではなく茅場町行きが出現したというのも少々おかしな話です

今回出現した茅場町行きは茅場町では折返しせず、東陽町まで回送されてから東陽町始発の列車として折返し運転を実施していました

茅場町→東陽町にて意図的に減便をしている理由については、代行バスの輸送量を鑑みてetc（葛西側と同様）の事と考えられます

であれば、大江戸線と乗り換え可能な門前仲町までは運転しても良かったのでは・・・？と今更ながらに気づいたのです

強いて言えば、極力自社線で迂回して貰いたいですとか、そのような感じでしょうか・・・

当日は振替乗車が可能ですから、門前仲町から大江戸線を利用して迂回する乗客を考慮すると、次回以降はぜひ門前仲町まで5分ヘッドを保って頂きたい所ですね

線路切替工事はあと2回ほど予定されていますが、今回の輸送量のデータを元にして、また違ったダイヤが組まれる可能性もあります

Part.4へ続きます
