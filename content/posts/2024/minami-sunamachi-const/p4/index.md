---
title: 第1回 南砂町駅線路切替工事 Part.4
date: 2024-05-12
categories: [東西線, 南砂町駅線路切替工事]
---

## 東武バスによるオリジナルの行先表示
![](20240512_152852.jpg)
![](20240512_154604.jpg)

筆者はバスについて全く明るくないため詳細な解説は省略いたしますが、バス代行の一番の目玉は東武バスでした

今回の代行バス実施については、都営、京成、京王、西武、東武、国際興業etc、と首都圏各地のバス会社によって行われましたが、各社では「貸切」「列車代行」といった汎用的な表示をしている中、東武バスはオリジナルの表示を用意していました

東陽町行きのバスには05系のイラストが、西葛西行きのバスに至ってはなんと5000系のイラストが表示されていました・・・東西線の5000系は引退してから15年以上経過しますが、それを起用してくるあたり流石です

職人さんがかなり張り切って制作したことが想像できます

## 京成バス
![](20240512_153324.jpg)
![](20240512_145408.jpg)

京成バスは汎用的な「貸切」表示を使用していましたが、オリジナルの札（？）が掲げられていました

また、側面に貸切と表記されたバスも器用されていました

各社どのバスも、平均的に綺麗な車体の物が多く見られましたが、貸切専用のバスは屋根までピカピカですね・・・

## 京王帝都・・・？
![](20240512_154331.jpg)

素人ながらに明らかに異質なものがやってきたと分かる、京王からはなんと「京王帝都」表記のバスが・・・

昔の塗装を復刻しました感がバチバチに出ていますね

数年前に都営バスでも復刻塗装のようなものが走っていましたが、それに近いものを感じました
## 空港リムジン参戦
![](20240512_150347.jpg)

これはもしかして普段の東西線よりも快適なのではないでしょうか、なんと東京空港交通からリムジンバスが参戦しました

立ち席も無いでしょうから、小池知事の提唱する満員電車ゼロへの布石となりますね！（？？）

繰り返しになりますがバスの方は知見が無い為、このあたりで切り上げたいと思います
