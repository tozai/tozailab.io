---
tags: [東京メトロ有楽町線, 東京メトロ副都心線, 東京メトロ千代田線, 東京メトロ南北線, 西武鉄道]
title: "新木場CR探訪" 
date: 2024-04-28
---

![](20240428_174715.jpg)
![](20240428_175000.JPG)

有楽町線の新木場車両基地へ赴いてきました

そして最初に現れたのは・・・メトロではなく西武の車両でした

この日は、40000系が2編成並んで留置されていました　しかも連番ですね！

下二桁が50番台となっていますが、40000系は0番台と50番台で車内の構造が異なるようですね

0番台はS-TRAINとして運用する為L/Cカーのような座席となっていますが、50番台は他の列車と同様に普通のロングシートを採用しているようです

ここで撮影して初めて気づきましたが、大型の窓（パートナーゾーンのある所）を採用しているのは10号車側の先頭車のみで、1号車は普通の窓のようですね・・・

パートナーゾーン、カタカナ語で新しい概念っぽい印象がありますが、ようは車椅子・ベビーカー用スペースの延長のような存在です

ドア間の椅子を取っ払って広いスペースを作り、真ん中には申し訳程度に腰掛けスペース（秋葉原駅前とかにある椅子ですらない何か）を設置したシロモノです

オールロングシートである50番台に採用するのはまだ分かるのですが、S-TRAINとして運行する0番台に採用するのは愚策に思います・・・せっかく指定席料金を取る設備を設けておいて、座席自体を減らすというのは矛盾しているような・・・

車端部の3席分あるロングシートはクロスシートへの転換が不可能な作りとなっていますが、それでしたらロングシート自体設置せず、こちらを車椅子スペースとした方が良いでしょう・・・

現に、中間車では車端部に車椅子スペースを設けていますし、奇をてらう事はせず中間車と同じ仕様で十分だったのでは？と考えています

と批判しつつも、五直の中ではトップクラスに格好いいな～と思います

あまり撮影機会に恵まれてこなかったので、良い機会となりました

![](20240428_184112.JPG)

奥の方に目をやると、南北線の9000系(9110F)が止まっていました

どうやら前日の27日に此処へ回送されたようで、これからB修繕工事に入ると思われます・・・

![](20240428_174306.JPG)
![](20240428_181904.JPG)

横から見ると車両がズラリ、壮観です

一番奥には千代田線の6000系(6102F)が、営団マークを貼り付けたまま留置されていました

引退してから5年以上経過しましたが、10両のまま動態保存(or静態保存)するのでしょうか？

10両フルで保存となると中々に手間がかかりそうですが、鉄道ファンにとっては勿論嬉しいので、是非とも維持して頂きたい所です

![](20240428_175646.JPG)
![](20240428_183613.JPG)

新木場CRのメインディッシュとなる、10000系や17000系

17000系は1編成のみ留置されていました

2形式を比較すると、10000系よりも新しい17000系はより丸みのあるデザインとなっていますね

IT業界でも段々、丸みを強調してきている印象があります　サードパーティによるカスタムの無い純正Android OSも、バージョンアップする毎にUI部品が丸くなっているように感じます

そこに拘りすぎて余白が増え、情報量が減ってきているのが個人的に不満なのですが、この風潮は今後も暫く続いていくことでしょう・・・

![](20240428_180325.JPG)
![](20240428_183127.JPG)

他にも、7000系(7101F)が通電状態で留置されているのを確認しました

こちらも10両編成のまま保存するのでしょうか・・・東京メトロの体力には驚かされますね

また、列車が出庫するタイミングと重なり、10000系が出ていく様子を丁度撮影することが出来ました

新木場CRにかかる歩道橋はある程度高さが有りますし、また横から角度を稼ごうとすると障害物が入りがちなことから、縦で撮影すると綺麗に収まりやすい印象でした

東武の車両に出くわすことが出来ませんでしたが、休日昼の留置は無いのでしょうか・・・？東武車の捕獲については、また別の日にチャレンジしてみます