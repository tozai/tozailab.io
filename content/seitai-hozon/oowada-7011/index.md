---
title: 大和田公園・都電7011号車
date: 2024-09-04
address: 千葉県市川市大和田1丁目15 大和田公園
googleMap: https://maps.app.goo.gl/wTs4hV2JDHmRwfJd7
memo: 公園の真下は都営新宿線が通っている
---

![](20240904_114957.jpg)
